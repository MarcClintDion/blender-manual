.. |gizmo-icon| image:: /images/editors_3dview_display_gizmo_header.png
.. _bpy.types.SpaceClipEditor.show_gizmo:

***************
Viewport Gizmos
***************

.. reference::

   :Mode:      All Modes
   :Header:    |gizmo-icon| :menuselection:`Gizmos`

Clicking the icon toggles all gizmos in the 3D Viewport.
The drop-down button displays a popover with more detailed settings,
which are described below.


Viewport Gizmos
===============

.. _bpy.types.SpaceClipEditor.show_gizmo_navigate:

Navigate
   Toggle the visibility of the gizmos in the upper right of the editor
   that are used to pan and zoom the 2D viewport.
