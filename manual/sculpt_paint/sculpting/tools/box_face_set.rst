
************
Box Face Set
************

.. reference::

   :Mode:      Sculpt Mode
   :Tool:      :menuselection:`Toolbar --> Box Face Set`

Creates a new :doc:`Face Set </sculpt_paint/sculpting/editing/face_sets>`
based on a :ref:`box selection <tool-select-box>`.


Tool Settings
=============

Front Faces Only
   Only creates a mask on the faces that face towards the view.
