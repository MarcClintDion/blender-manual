
************
Introduction
************

Sculpting and painting offers a more freeform workflow of editing via :doc:`brushes </sculpt_paint/brush/index>`.
There are several :doc:`modes </editors/3dview/modes>` to do this, each with their own purpose.

- :doc:`Sculpting </sculpt_paint/sculpting/introduction/general>`:
  Change and transform the topology of your mesh.
- :doc:`Vertex Paint </sculpt_paint/vertex_paint/introduction>`:
  Change the color of vertices in the active Color Attribute.
- :doc:`Weight Paint </sculpt_paint/weight_paint/introduction>`:
  Change the weight of vertices in the active vertex group.
- :doc:`Texture Paint </sculpt_paint/texture_paint/introduction>`:
  Change the pixels of the active image texture.
