.. index:: Transform; Modeling Transform
.. index:: Modeling Transform

************
Introduction
************

Transform is the modality of operations that perform transformations in 2D and 3D elements.
Transformations can include things like moving, rotating, scaling,
and applying other operations to objects in the scene.

They work by changing the geometry which you can edit directly.


Operators
=========

.. _bpy.ops.transform.transform:

There are several transformation operations included in Blender.
Here are some of the main operations available:


.. _bpy.ops.transform.translate:

Move
----

This operations allows you to move elements along the X, Y, and Z axes in the scene.


.. _bpy.ops.transform.rotate:

Rotate
------

You can use this function to rotate elements around the X, Y, and Z axes.


.. _bpy.ops.transform.resize:

Scale
-----

Scaling allows you to increase or decrease the size of an object along the X, Y, and Z axes.


.. _bpy.ops.transform.align:

Align to View
-------------

This is useful for aligning objects with the view from the camera or another specific viewpoint.


.. _bpy.ops.transform.mirror:

Mirror
------

Mirrors objects along one or more axes.
